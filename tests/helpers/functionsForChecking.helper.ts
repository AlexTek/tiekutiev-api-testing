import { expect } from "chai";


export function checkStatusCode(response, statusCode: 200 | 201 | 204 | 400 | 401 | 403 | 404 | 409 | 500) {
    expect(response.statusCode, `Status Code should be ${statusCode}`).to.equal(statusCode);
}

export function checkResponseTime(response, maxResponseTime: number = 3000) {
    expect(response.timings.phases.total, `Response time should be less than ${maxResponseTime}ms`).to.be.lessThan(
        maxResponseTime
    );
}
export function checkJsonSchema(response, jsonSchema) {
    expect(response.body, `Json schema is correct`).to.be.jsonSchema(jsonSchema);
}
export function checkValidationErrorMessage(response) {
    expect(response.body.message).to.be.equal('Validation errors')
}
export function checkResponseLenghtsMoreThanOne(response) {
    expect(response.body.length, `Response body should have more than 1 item`).to.be.greaterThan(1);
} 
export function checkUserData(response, avatar, userName, email, userId) {
    expect(response.body.avatar).to.be.equal(avatar);
    expect(response.body.userName).to.be.equal(userName);
    expect(response.body.email).to.be.equal(email);
    expect(response.body.id).to.be.equal(userId);
} 
export function checkUserDataAfterRegistration(response, avatar: string, userName: string, email: string, userId: number) {
    expect(response.body.user.avatar).to.be.equal(avatar);
    expect(response.body.user.userName).to.be.equal(userName);
    expect(response.body.user.email).to.be.equal(email);
    expect(response.body.user.id).to.be.equal(userId);
} 
export function checkNewCreatedPostData(response, userId, previewImage, bodyValue){
    expect(response.body.author.id).to.be.equal(userId);
    expect(response.body.previewImage).to.be.equal(previewImage);
    expect(response.body.body).to.be.equal(bodyValue);
}
export function checkNewLikeFromUser(response, userId, postId){
    let postsByLoggedUser  = response.body.filter((post) => post.id === postId);
       let likedPostByLoggedUser = postsByLoggedUser[0].reactions.filter((user) => user.user.id === userId);
       expect(likedPostByLoggedUser.length, `Reaction is missing`).to.be.greaterThan(0);
       expect(likedPostByLoggedUser[0].isLike).to.be.equal(true);
       expect(likedPostByLoggedUser[0].user.id).to.be.equal(userId)
}
export function checkNewCreatedComment(response,userId, postId, bodyValueComment){
    let postsByLoggedUser  = response.body.filter((post) => post.id === postId);
    let commentedPostByLoggedUser = postsByLoggedUser[0].comments.filter((user) => user.author.id === userId);
    expect(commentedPostByLoggedUser[0].body,"Check comment is correct").to.be.equal(bodyValueComment);
    expect(commentedPostByLoggedUser[0].author.id).to.be.equal(userId)
} 
export function checkErrorCode(response, errorMessage: "An error occurred while saving the entity changes. See the inner exception for details."|
'Entity User was not found.'|'Invalid username or password.',code:1 | 2 | 3) {
    expect(response.body.error, `Error message - ${errorMessage}`).to.be.equal(errorMessage);
    expect(response.body.code, `Error code  - ${code}`).to.be.equal(code);
    
} 
// export function checkUserDataFromAllUsers (response, )
    