import { ApiRequest } from "../request";

const baseUrl: string = global.appConfig.baseUrl

export class RegistrationController {
    
    async userRegistration(image: string, emailValue: string,userNameValue: string,passwordValue: string) {
        const response = await new ApiRequest()
            .prefixUrl(baseUrl)
            .method("POST")
            .url(`api/register`)
            .body({
            "avatar": image,
            "email":emailValue,
            "userName":userNameValue,
            "password":passwordValue
    })
            .send();
        return response;
    }
}
