import { ApiRequest } from "../request";

const baseUrl: string = global.appConfig.baseUrl

export class PostsController {
    async getAllPosts() {
        const response = await new ApiRequest()
            .prefixUrl(baseUrl)
            .method("GET")
            .url(`api/posts`)
            .send();
        return response;
    }
    async createNewPost(authorId: Number, previewImage: string, bodyValue: string, accessToken: string) {
        const response = await new ApiRequest()
            .prefixUrl(baseUrl)
            .method("POST")
            .url(`api/posts`)
            .body({
                "authorId": authorId,
                "previewImage": previewImage,
                "body": bodyValue
            })
            .bearerToken(accessToken)
            .send();
        return response;
    }
    async likeReaction(postId:number,userId:number, accessToken:string, isLike: Boolean) {
        const response = await new ApiRequest()
            .prefixUrl(baseUrl)
            .method("POST")
            .url(`api/posts/like`)
            .body ({
            "entityId": postId,
            "isLike": isLike,
            "userId": userId})
            .bearerToken(accessToken)
            .send();
        return response;
    }
}
