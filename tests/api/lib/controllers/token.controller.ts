import { ApiRequest } from "../request";    

const baseUrl: string = global.appConfig.baseUrl

export class TokenController {
    async updateToken(accessToken, refreshToken) {
        const response = await new ApiRequest()
            .prefixUrl(baseUrl)
            .method("POST")
            .url(`api/token`)
            .body({
                "accessToken": accessToken,
                "refreshToken": refreshToken 
    })
            .bearerToken(accessToken)
            .send();
        return response;
    };
    async revokeToken( refreshToken, accessToken) {
        const response = await new ApiRequest()
            .prefixUrl(baseUrl)
            .method("POST")
            .url(`api/token`)
            .body({
"refreshToken": refreshToken 
             } )
            .bearerToken(accessToken)
            .send();
        return response;
    };
}