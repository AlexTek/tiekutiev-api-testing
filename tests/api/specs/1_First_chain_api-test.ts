import { expect } from "chai";
import { UsersController } from "../lib/controllers/users.controller";
import { RegistrationController } from "../lib/controllers/registration.controller";
import { AuthController } from "../lib/controllers/auth.controller";
import { checkJsonSchema, checkStatusCode, checkUserDataAfterRegistration, checkUserData, checkResponseTime } from "../../helpers/functionsForChecking.helper";

const schema_registrUser = require("./data/schemas_registrUser.json");
const schema_allUsers = require("./data/schemas_allUsers.json")

const email = global.appConfig.users.firstUser.email;
const avatar = global.appConfig.users.firstUser.avatar;
const userName = global.appConfig.users.firstUser.userName;
const password = global.appConfig.users.firstUser.password;
const updatedEmail = "Updated@Email.com";
const updatedavatar = "updatedavatar";
const updatedUsername = "updatedusername";
const auth = new AuthController();
const users = new UsersController();
const userRegistration = new RegistrationController();
const chai = require('chai');
chai.use(require('chai-json-schema'));

describe ("New user registration", () => {
    let loginToken: string;
    let userId: number;
        it("Should register a new user", async () => {
    const response = await userRegistration.userRegistration(avatar, email, userName, password);
    userId = response.body.user.id;
    checkStatusCode(response, 201);
       checkJsonSchema(response, schema_registrUser)
       checkUserDataAfterRegistration(response, avatar, userName, email, userId)
     
 });
     it("Get all user list", async () => {
    const response = await users.getAllUsers();
    checkJsonSchema(response, schema_allUsers)
    checkStatusCode(response, 200);
    checkResponseTime(response,1000);
});
    it("Log in with valid credentials", async () => {
    const response = await auth.login(email, password);
    loginToken = response.body.token.accessToken.token;
    userId = response.body.user.id;
});
it("Get details of the current logged in user by token", async () => {
    const response = await users.getUserbyToken(loginToken);
    checkStatusCode(response, 200);
    checkResponseTime(response,1000);

    checkUserData(response, avatar, userName, email, userId);
});

it("Update current user", async () => {
    let updatedUserData: object = {
        avatar: updatedavatar,
        email: updatedEmail,
        userName: updatedUsername,
        id: userId,
    };
    const response = await users.updateUser(updatedUserData, loginToken);
    checkStatusCode(response, 204);
    checkResponseTime(response, 1000)
});

it("Get details of the current logged-in user", async () => {
    const response = await users.getUserbyToken(loginToken);
    checkStatusCode(response, 200);
    checkResponseTime(response, 1000);
    checkUserData(response, updatedavatar, updatedUsername, updatedEmail, userId);
});

it("Get details of current user by its id:", async () => {
    const response = await users.getUserbyId(userId);
    checkStatusCode(response, 200);
    checkResponseTime(response, 1000)
    checkUserData(response, updatedavatar, updatedUsername, updatedEmail, userId);
});
it("Delete current user", async () => {
    const response = await users.deleteUser(userId, loginToken);
    checkStatusCode(response, 204);
   checkResponseTime(response, 1000);
});
});

