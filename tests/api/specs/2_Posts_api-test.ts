import { expect } from "chai";
import { PostsController } from "../lib/controllers/posts.controller";
import { UsersController } from "../lib/controllers/users.controller";
import { RegistrationController } from "../lib/controllers/registration.controller";
import { CommentsController } from "../lib/controllers/comments.controller";
import { checkJsonSchema, checkResponseLenghtsMoreThanOne, checkResponseTime, checkStatusCode, checkNewCreatedPostData, checkNewLikeFromUser, checkNewCreatedComment} from "../../helpers/functionsForChecking.helper";

const avatar = global.appConfig.users.firstUser.avatar;
const userName = global.appConfig.users.firstUser.userName;
const posts = new PostsController();
const comments = new CommentsController();
const users = new UsersController();
const userRegistration = new RegistrationController();
const email = global.appConfig.users.firstUser.email;
const password = global.appConfig.users.firstUser.password;
const schema_allUsers = require("./data/schemas_allUsers.json")
const chai = require("chai");
chai.use(require("chai-json-schema"));
const previewImage = "testimage";
const bodyValue = "BodyValueNewPost";
const bodyValueComment = "BodyValueComment";

describe(`Test post functionality`, () => {
    let loginToken: string;
    let userId: number;
    let postId: number;
    let commentId: number;

    before(`Registration, get the token and userId`, async () => {
        const response = await userRegistration.userRegistration(avatar, email, userName, password);
        userId = response.body.user.id;
        loginToken = response.body.token.accessToken.token;
    });

    it(`Return All Posts`, async () => {
        let response = await posts.getAllPosts();
        checkStatusCode(response, 200);
        checkResponseTime(response, 1500);
        checkJsonSchema(response, schema_allUsers)
    });

    it("Create new post", async () => {
        let response = await posts.createNewPost(userId, previewImage, bodyValue, loginToken);
        postId = response.body.id;
        checkStatusCode(response, 200);
        checkNewCreatedPostData(response, userId, previewImage, bodyValue);
    });

    it("Add like reaction to post", async () => {
        let response = await posts.likeReaction(postId, userId, loginToken, true);
       checkStatusCode(response, 200);
       let allPosts = await posts.getAllPosts();
       checkNewLikeFromUser(allPosts, userId, postId)
    });

    it("Add new comment to post", async () => {
        let response = await comments.createNewComments(userId, postId, bodyValueComment, loginToken);
        checkStatusCode(response, 200);
        expect(response.body.author.id).to.be.equal(userId);
        expect(response.body.body).to.be.equal(bodyValueComment);
        let allPosts = await posts.getAllPosts();
        checkNewCreatedComment(allPosts, userId, postId, bodyValueComment)
    });
    after("Delete registrated user", async () => {
        const response = await users.deleteUser(userId, loginToken);
        checkStatusCode(response, 204);
        checkResponseTime(response, 1000);
    });
});
