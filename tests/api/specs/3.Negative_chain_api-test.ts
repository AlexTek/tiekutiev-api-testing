import { expect } from "chai";
import { PostsController } from "../lib/controllers/posts.controller";
import { UsersController } from "../lib/controllers/users.controller";
import { RegistrationController } from "../lib/controllers/registration.controller";
import { checkJsonSchema,  checkErrorCode, checkResponseLenghtsMoreThanOne,
     checkResponseTime, checkStatusCode, checkNewCreatedPostData, checkNewLikeFromUser, checkNewCreatedComment} from "../../helpers/functionsForChecking.helper";


const avatar = global.appConfig.users.firstUser.avatar;
const userName = global.appConfig.users.firstUser.userName;
const userRegistration = new RegistrationController();
const email = global.appConfig.users.firstUser.email;
const password = global.appConfig.users.firstUser.password;
const users = new UsersController();
const previewImage = "testimage";
const bodyValue = "BodyValueNewPost";
const posts = new PostsController();
const schema_allUsers = require("./data/schemas_allUsers.json")
const chai = require("chai");
chai.use(require("chai-json-schema"));


describe("Check that deleted post cannot be liked", () => {
    let loginToken: string;
    let userId: number;
    let postId: number;

    it(`Registration, get the token and userId`, async () => {
        const response = await userRegistration.userRegistration(avatar, email, userName, password);
        userId = response.body.user.id;
        loginToken = response.body.token.accessToken.token;
    });
    it("Create new post", async () => {
        let response = await posts.createNewPost(userId, previewImage, bodyValue, loginToken);
        postId = response.body.id;
        checkStatusCode(response, 200);
        checkNewCreatedPostData(response, userId, previewImage, bodyValue);
    });
    it("Delete current user", async () => {
        const response = await users.deleteUser(userId, loginToken);
        checkStatusCode(response, 204);
       checkResponseTime(response, 1000);
    });
    it(`Registration, get the token and userId`, async () => {
        const response = await userRegistration.userRegistration(avatar, email, userName, password);
        userId = response.body.user.id;
        loginToken = response.body.token.accessToken.token;
    });
    it("Try Add like reaction to deleted post", async () => {
        let response = await posts.likeReaction(postId, userId, loginToken, true);
       checkStatusCode(response, 500);
       checkErrorCode(response,"An error occurred while saving the entity changes. See the inner exception for details.",1);
    });
    }
);